# FastAPI and MLflow Integration

## Overview
This application demonstrates an integration of FastAPI with MLflow. It uses MLflow to track models, and a FastAPI server to serve predictions from a trained model. The app also includes a Jupyter notebook for data exploration and model training.

## Services

1. **Jupyter**: For data exploration and model training.
2. **Postgres**: Database backend for MLflow.
3. **S3 (MinIO)**: S3-compatible storage for model artifacts.
4. **MLflow**: Model tracking and management.
5. **FastAPI**: API endpoint for model predictions.

## Getting Started

### Prerequisites

- Docker and Docker-Compose installed.

### Setup

1. Clone the repository.
2. Navigate to the project directory.
3. Run `docker-compose up` to start all services.

### API Usage

Make a POST request to `http://localhost:8000/predict` with input data in the format:

```json
{
    "features": [...]
}
```

The API will respond with predictions.

Example:

 ```bash
 curl -X POST "http://localhost:8000/predict" -H "accept: application/json" -H "Content-Type: application/json" -d '{"features": {"symboling": 3, "normalized-losses": "nan", "make": "alfa-romero", "fuel-type": "gas", "aspiration": "std", "num-of-doors": "two", "body-style": "convertible", "drive-wheels": "rwd", "engine-location": "front", "wheel-base": 88.6, "length": 168.8, "width": 64.1, "height": 48.8, "curb-weight": 2548, "engine-type": "dohc", "num-of-cylinders": "four", "engine-size": 130, "fuel-system": "mpfi", "bore": 3.47, "stroke": 2.68, "compression-ratio": 9.0, "horsepower": 111.0, "peak-rpm": 5000.0, "city-mpg": 21}}'
 ```

Output: {"prediction":[13951.274475139522]}

### Notebooks
Open Jupyter at `http://localhost:8888` and navigate to the `notebooks` directory. Use the `model_eda_train.ipynb` notebook for data exploration and model training.