#!/bin/bash

# Start Gunicorn processes
echo Starting Gunicorn.
exec gunicorn -k uvicorn.workers.UvicornWorker -b 0.0.0.0:8000 --log-level info app:app
