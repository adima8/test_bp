from fastapi import FastAPI, HTTPException, Request
import mlflow.pyfunc
import pandas as pd
import logging
import os

app = FastAPI()

# Load model from MLflow Model Registry
# Replace 'models:/<model_name>/<stage>' with the actual model path in the MLflow Model Registry]
os.environ['AWS_ACCESS_KEY_ID'] = 'IAM_ACCESS_KEY'
os.environ['AWS_SECRET_ACCESS_KEY'] = 'IAM_SECRET_KEY'
os.environ['MLFLOW_S3_ENDPOINT_URL'] = 'http://s3:9000'
mlflow.set_tracking_uri("http://mlflow:5000")
# set model_uri to the model path in the MLflow Model Registry
 

model_uri = "models:/car_price/Production"

logging.info(f'Loading model from URI: {model_uri}')

model = mlflow.pyfunc.load_model(model_uri)

@app.post("/predict")
async def predict(request: Request):
    # Parse input features from the request
    logging.info("Starting processing")
    input_data = await request.json()
    print(f'input_data: {input_data}')
    features = input_data["features"]

    # get model expected features
    features_names = model._model_impl.sklearn_model.feature_names_

    # Validate features
    if not features:
        raise HTTPException(status_code=400, detail="Invalid input, 'features' key is missing or empty.")

    # Convert features to DataFrame (assuming it's a 2D array)
    df = pd.DataFrame(features, columns=features_names, index=[0])

    # Get model prediction
    try:
        prediction = model.predict(df)
        return {"prediction": prediction.tolist()}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
